# WH-2019-nCoV

## 说明

本次项目目的是针对本次WH新型肺炎疫情的数据分析.

信息来源要求:

1. 来源可靠
2. 数据获取相对容易
3. 数据信息量充足

项目要求:

1. 不得虚假传播信息
2. 稳定居民当前情绪
3. 公开公正处理



## 信息来源

* [丁香医生实时数据](https://3g.dxy.cn/newh5/view/pneumonia?from=timeline&isappinstalled=0)
* [其他来源待添加]()



## 消息获取

* [人民网](<�meta charset='utf-8'><�a href="http://www.people.com.cn/">http://www.people.com.cn/<�/a>)
* [微博](<�meta charset='utf-8'><�a href="https://m.weibo.cn/?sudaref=www.baidu.com&amp;display=0&amp;retcode=6102">https://m.weibo.cn/?sudaref=www.baidu.com&amp;display=0&amp;retcode=6102<�/a>)

* [丁香医生实时数据](https://3g.dxy.cn/newh5/view/pneumonia?from=timeline&isappinstalled=0)

